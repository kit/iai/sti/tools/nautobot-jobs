from nautobot.apps.jobs import Job, register_jobs


class GenerateDaCable(Job):
    class Meta:
        name = "Create Multiple Circuits for a cable"
        description = """
            This job creates multiple Circuits at once
        """
        has_sensitive_variables = False

    def run(self, *args, **kwargs):
        pass


register_jobs(GenerateDaCable)
